# shipmanifest

Python tool to manage a ship manifest in pen and paper role playing games

Licensed under BSD3 license.

Only tested under Windows 10 with anaconda Python.

## Requirements:

- python 3.8
- kivy
- pandas
- numpy
- tabulate
- xlwt
- openpyxl