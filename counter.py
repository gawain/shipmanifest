#!/usr/bin/env python


__author__ = "Sir Gawain"
__copyright__ = "Copyright 2020 by Sir Gawain"
__credits__ = []
__maintainer__ = "Sir Gawain"
__status__ = "Prototype"
__date__ = "25.04.2021"
__project__ = "ship manifest"
__license__ = 'BDS 3 Clause New or Revised License'
__born__ = "On 25.04.2021 at 11:51 as 'counter.py'"


# obsolete as no counter is needed at the moment.
# Was used as an alternative uuid generator. Kept for a future feature.


class Counter:
    '''
    Class that counts only positively. Can never be negative.
    Can be used to track instances or such.
    '''

    def __init__(self):
        self.__cnt = 0

    def increase(self):
        self.__cnt += 1

        return self.__cnt

    def decrease(self):
        self.__cnt -= 1
        if self.__cnt < 0:
            self.__cnt = 0

        return self.__cnt

    def __repr__(self):
        return f'{self.__cnt}'

    def __eq__(self, other):
        return self.__cnt == other

    def __le__(self, other):
        return self.__cnt <= other

    def __ge__(self, other):
        return self.__cnt >= other

    def __lt__(self, other):
        return self.__cnt < other

    def __gt__(self, other):
        return self.__cnt > other