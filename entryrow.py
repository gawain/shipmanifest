#!/usr/bin/env python

from kivy.clock import Clock
from kivy.properties import ObjectProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from states import UpdateState, OperatorState


__author__ = "Sir Gawain"
__copyright__ = "Copyright 2020 by Sir Gawain"
__credits__ = []
__maintainer__ = "Sir Gawain"
__status__ = "Prototype"
__date__ = "25.04.2021"
__project__ = "ship manifest"
__license__ = 'BDS 3 Clause New or Revised License'
__born__ = "On 25.04.2021 at 11:50 as 'entryrow.py'"



class EntryRow(BoxLayout):
    name_field = ObjectProperty(None)
    density_field = ObjectProperty(None)
    weight_field = ObjectProperty(None)
    volume_field = ObjectProperty(None)
    comment_field = ObjectProperty(None)
    delete_button = ObjectProperty(None)

    name_v = ObjectProperty("")
    density_v = ObjectProperty(0)  # kg/cm^3 or t/m^3
    weight_v = ObjectProperty(0)   # t
    volume_v = ObjectProperty(0)   # m^3
    comment_v = ObjectProperty("")

    ondelete = ObjectProperty(None)

    tablemain = ObjectProperty(None)

    def __init__(self, row_id, **kwargs):
        super().__init__(**kwargs)
        self.row_id = row_id

        self.name_field.text = self.name_v
        self.weight_field.update_text(self.weight_v)
        self.volume_field.update_text(self.volume_v)
        self.density_field.update_text(self.density_v)
        self.comment_field.text = self.comment_v

        self.delete_button.bind(on_press=lambda x: self.ondelete(self))

        Clock.schedule_once(self.set_focus_first, 0)

    def set_focus_first(self, *args):
        self.name_field.focus = True

    def apply_ops(self, old, v, ops):
        if ops is OperatorState.add:
            return old+v
        if ops is OperatorState.sub:
            return max(old-v, 0)
        return v

    def __calc_volume(self, weight, density):
        return weight / density

    def __calc_weight(self, volume, density):
        return volume * density

    def update_textfields(self, state, v):
        if state is UpdateState.name:
            self.name_v = v
        if state is UpdateState.comment:
            self.comment_v = v

    def update_values(self, state, v, ops):

        density = self.density_v
        if density < 1e-5:
            density = 1

        weight = self.weight_v
        volume = self.volume_v

        if state is UpdateState.density:
            density = self.apply_ops(density, v, ops)

            if abs(density-1) > 1e-5 and weight > 1e-10 and volume > 1e-5:
                weight = self.__calc_weight(volume, density)

            elif weight > 1e-10:
                volume = self.__calc_volume(weight, density)

            elif volume > 1e-5:
                weight = self.__calc_weight(volume, density)

        elif state is UpdateState.weight:
            weight = self.apply_ops(weight, v, ops)
            volume = self.__calc_volume(weight, density)

        elif state is UpdateState.volume:
            volume = self.apply_ops(volume, v, ops)
            weight = self.__calc_weight(volume, density)

        self.volume_field.update_text(volume)
        self.density_field.update_text(density)
        self.weight_field.update_text(weight)

        self.weight_v = weight
        self.volume_v = volume
        self.density_v = density

        self.tablemain.update_summary()

        # print(self.weight_v)
        # print(self.comment_v)
        # print(self.name_v)
        # print(self.density_v)
        # print(self.volume_v)
