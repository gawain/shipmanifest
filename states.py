#!/usr/bin/env python

import enum

__author__ = "Sir Gawain"
__copyright__ = "Copyright 2020 by Sir Gawain"
__credits__ = []
__maintainer__ = "Sir Gawain"
__status__ = "Prototype"
__date__ = "25.04.2021"
__project__ = "ship manifest"
__license__ = 'BDS 3 Clause New or Revised License'
__born__ = "On 25.04.2021 at 11:53 as 'states.py'"


class UpdateState(enum.Enum):
    weight = 0
    volume = 1
    density = 2
    name = 3
    comment = 4


class OperatorState(enum.Enum):
    add = 0
    sub = 1
    replace = 2