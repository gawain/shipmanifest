#!/usr/bin/env python

import re

from kivy.uix.button import Button
from kivy.uix.textinput import TextInput

from states import UpdateState, OperatorState


__author__ = "Sir Gawain"
__copyright__ = "Copyright 2020 by Sir Gawain"
__credits__ = []
__maintainer__ = "Sir Gawain"
__status__ = "Prototype"
__date__ = "25.04.2021"
__project__ = "ship manifest"
__license__ = 'BDS 3 Clause New or Revised License'
__born__ = "On 25.04.2021 at 11:51 as 'fields.py'"


class FloatInput(TextInput):
    pat = re.compile('[^-+0-9]')

    def insert_text(self, substring, from_undo=False):
        pat = self.pat

        if '.' in self.text:
            s = re.sub(pat, '', substring)
        else:
            s = '.'.join([re.sub(pat, '', s) for s in substring.split('.', 1)])

        return super(FloatInput, self).insert_text(s, from_undo=from_undo)

    def on_touch_down(self, touch):
        if super().on_touch_down(touch):
            self.text = ""
            return True
        else:
            return False


class DeleteButton(Button):
    pass


class DensityField(FloatInput):
    pat = re.compile('[^-+0-9]')

    def format_text(self):

        s = self.text.strip()

        if ' ' in s:
            s = s.lstrip()
            s = s.split(' ')[0]

        s = s.strip()
        f = 1
        state = OperatorState.replace

        if s.startswith('+'):
            state = OperatorState.add
            s = s.replace('+', '')

        elif s.startswith('-'):
            state = OperatorState.sub
            s = s.replace('-', '')

        if s == '':
            s = 0
            state = OperatorState.add

        try:
            s = float(s)*f
        except:
            print('error')

        return s, state

    def on_text_validate(self):
        s, state = self.format_text()
        self.parent.update_values(UpdateState.density, s, state)

    def on_focus(self, instance, value):
        if not value:
            self.on_text_validate()

    def update_text(self, v):
        x1, x2 = f'{v:10.3f}'.split('.')
        if int(x2) == 0:
            self.text = f'{v:7,.0f} t/m^3'
        else:
            self.text = f'{v:7,.3f} t/m^3'


class WeightField(FloatInput):
    pat = re.compile('[^-+0-9tkg]')

    def format_text(self):
        s = self.text.strip()

        # FIXME > introduced some bugs but not yet investigated.
        # if ' ' in s:
        #     s = s.lstrip()
        #     s = s.split(' ')[0]

        s = s.strip()
        f = 1
        state = OperatorState.replace

        if s.startswith('+'):
            state = OperatorState.add
            s = s.replace('+', '')

        elif s.startswith('-'):
            state = OperatorState.sub
            s = s.replace('-', '')

        if s.endswith('t') or s.endswith('T'):
            s = s.replace('t', '').replace('T','')

        elif s.endswith('kg'):
            s = s.replace('kg', '')
            f = 1e-3

        if s=='':
            s = 0
            state = OperatorState.add

        try:
            s = float(s)*f
        except:
            print('error')

        return s, state

    def on_text_validate(self):
        s, state = self.format_text()
        self.parent.update_values(UpdateState.weight, s, state)

    def on_focus(self, instance, value):
        if not value:
            self.on_text_validate()

    def update_text(self, v):
        x1, x2 = f'{v:10.3f}'.split('.')
        if int(x2) == 0:
            self.text = f'{v:7,.0f} t'
        elif int(x1) == 0:
            self.text = f'{v*1e3:7,.0f} kg'
        elif int(x1) >0 and int(x2) == 0:
            self.text = f'{v:7,.0f} t'
        else:
            self.text = f'{v:7,.3f} t'


class VolumeField(FloatInput):
    pat = re.compile('[^-+0-9l]')

    def format_text(self):

        s = self.text.strip()

        if ' ' in s:
            s = s.lstrip()
            s = s.split(' ')[0]

        s = s.strip()
        f = 1
        state = OperatorState.replace

        if s.startswith('+'):
            state = OperatorState.add
            s = s.replace('+', '')

        elif s.startswith('-'):
            state = OperatorState.sub
            s = s.replace('-', '')

        if s.endswith('l') or s.endswith('L'):
            s = s.replace('l', '').replace('L','')

        if s=='':
            s = 0
            state = OperatorState.add

        try:
            s = float(s)*f
        except:
            print('error')

        return s, state

    def on_text_validate(self):
        s, state = self.format_text()
        self.parent.update_values(UpdateState.volume, s, state)

    def on_focus(self, instance, value):
        if not value:
            self.on_text_validate()

    def update_text(self, v):
        x1, x2 = f'{v:10.10f}'.split('.')
        if int(x2) == 0:
            self.text = f'{v:7,.0f} m^3'
        else:
            self.text = f'{v:7,.2f} m^3'


class NameField(TextInput):
    def format_text(self):
        return self.text, None

    def on_text_validate(self):
        s, state = self.format_text()
        self.parent.update_textfields(UpdateState.name, s)

    def on_focus(self, instance, value):
        if not value:
            self.parent.update_textfields(UpdateState.name, self.text)


class CommentField(TextInput):
    def format_text(self):
        return self.text, None

    def on_text_validate(self):
        s, state = self.format_text()
        self.parent.update_textfields(UpdateState.comment, s)

    def on_focus(self, instance, value):
        if not value:
            self.parent.update_textfields(UpdateState.comment, self.text)
