#!/usr/bin/env python

import pandas as pd

import pathlib as pt
import shutil

import uuid


__author__ = "Sir Gawain"
__copyright__ = "Copyright 2020 by Sir Gawain"
__credits__ = []
__maintainer__ = "Sir Gawain"
__status__ = "Prototype"
__date__ = "25.04.2021"
__project__ = "ship manifest"
__born__ = "On 25.04.2021 at 20:32 as 'converter.py'"


'''
This is a small converter script to convert an ods file into a json readable by ship manifest.
It should serve as an example on how to get data into the gui from an excel like format.
'''

p = pt.Path(r'D:\RPG\01_Deadlands\01_AsiaAdventure\02_Part2\fright.ods')

out = pt.Path('./data.json')

df = pd.read_excel(p.as_posix(), skiprows=3).iloc[:, 0:3]
df.columns = ['name', 'density', 'volume']
df['weight'] = df['density'] * df['volume']
df['comment'] = ''
df['id'] = [uuid.uuid4() for i in range(len(df))]
df = df.set_index('id')
df.index.name = None
df = df.dropna()

for after, current in [('.5', '.4'), ('.4', '.3'), ('.3', '.2'), ('.2', '.1'), ('.1', '')]:
    after = pt.Path(f'./data{after}.json')
    current = pt.Path(f'./data{current}.json')
    if current.exists():
        shutil.copyfile(current.as_posix(), after.as_posix())

df.to_json(out.as_posix(), indent=4)


