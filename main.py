#!/usr/bin/env python

from kivy.clock import Clock
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.config import Config
from kivy.core.window import Window
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.popup import Popup
from kivy.core.window import ObjectProperty
from kivy.uix.label import Label
from kivy.factory import Factory
from kivy.uix.scrollview import ScrollView

# required for kv file to work
from fields import *

import pandas as pd
import os
from datetime import datetime
import uuid

import shutil
import pathlib as pt
import numpy as np
import zipfile
import json

from entryrow import EntryRow


__author__ = "Sir Gawain"
__copyright__ = "Copyright 2020 by Sir Gawain"
__credits__ = []
__maintainer__ = "Sir Gawain"
__status__ = "Prototype"
__date__ = "25.04.2021"
__project__ = "ship manifest"
__license__ = 'BDS 3 Clause New or Revised License'
__born__ = "On 24.04.2021 at 13:22 as 'main.py'"


# for debugging uncomment to get better output of pd dataframes
# pd.set_option('display.max_rows', 500)
# pd.set_option('display.max_columns', 500)
# pd.set_option('display.width', 1000)


def create_empty_record(id):
    '''
    This creates an initialized pandas series which represents one record in the manifest.
    :param id: the id of the row. A uid. Can be a number or a string.
    '''
    return pd.Series(
        ['', 1.0, 0.0, 0.0, ''],
        index=[
            'name',
            'density',
            'volume',
            'weight',
            'comment',
        ],
        name=id,
    )


class Menubar(BoxLayout):
    main = ObjectProperty(None)
    max_weight_display = ObjectProperty(None)
    max_volume_display = ObjectProperty(None)


class Statusbar(Label):
    main = ObjectProperty(None)


class SaveDialog(FloatLayout):
    onexport = ObjectProperty(None)
    filename = ObjectProperty(None)
    oncancel = ObjectProperty(None)


class SaveDialog(FloatLayout):
    onsave = ObjectProperty(None)
    filepath = ObjectProperty(None)
    oncancel = ObjectProperty(None)


class LoadDialog(FloatLayout):
    onload = ObjectProperty(None)
    oncancel = ObjectProperty(None)


class BorderedLabel(Label):
    pass


class TableWidget(BoxLayout):
    rows_container = ObjectProperty(None)
    main = ObjectProperty(None)

    table_summary_bar = ObjectProperty(None)
    table_header = ObjectProperty(None)

    total_volume_display = ObjectProperty(None)
    total_weight_display = ObjectProperty(None)
    max_volume_display = ObjectProperty(None)
    max_weight_display = ObjectProperty(None)

    def add_row(self, **kwargs):
        row_id = uuid.uuid4().hex

        if len(kwargs) == 0:
            row = EntryRow(row_id, tablemain=self, ondelete=self.delete_row)
        else:
            row = EntryRow(row_id, tablemain=self, ondelete=self.delete_row, **kwargs)

        self.rows_container.add_widget(row)

    def delete_row(self, row):
        self.rows_container.remove_widget(row)
        del row

    def clear(self):
        self.rows_container.clear_widgets()

    def update_summary(self):
        v = np.sum([i.volume_v for i in self.rows_container.children])
        w = np.sum([i.weight_v for i in self.rows_container.children])

        self.total_volume_display.text = f'{v:10.0f} m^3'
        self.total_weight_display.text = f'{w:10.0f} T'

    def data(self):
        d = []
        for i in reversed(self.rows_container.children):
            rec = [i.row_id,
                   i.ids.name_field.format_text()[0],
                   i.ids.density_field.format_text()[0],
                   i.ids.volume_field.format_text()[0],
                   i.ids.weight_field.format_text()[0],
                   i.ids.comment_field.format_text()[0],
                   ]
            d.append(rec)

        d = pd.DataFrame(d, columns=['id', 'name', 'density', 'volume', 'weight', 'comment'])
        d = d.set_index('id')
        d.index.name = None

        return d


class CargoManifestEditor(BoxLayout):
    '''
    Main window widget of the cargo manifest gui.
    Doubles as a controller for most of the gui.
    '''

    # These are paths to the data files ship manifest uses internally to store data.
    # Ship manifest keeps a ledger of the last 5 saved files and a copy of the file at gui start.
    backup_patterns = ['.1.json', '.2.json', '.3.json', '.4.json', '.5.json']
    backup_pattern_on_start = pt.Path('.start.json')

    orientation = 'vertical'

    dataset = pd.DataFrame(columns=['name', 'density', 'volume', 'weight', 'comment'])

    def __init__(self, **kwargs):
        super(CargoManifestEditor, self).__init__(**kwargs)

        self.filepath = pt.Path('./data.zip')

        self._menu_bar = Menubar(main=self)
        self._status_bar = Statusbar(main=self)
        self._body = TableWidget(main=self)

        self.add_widget(self._menu_bar)
        self.add_widget(self._body)
        self.add_widget(self._status_bar)

        self._active_popup = None
        self._super = []

        keyboard = Window.request_keyboard(self._keyboard_released, self)
        keyboard.bind(on_key_down=self._keyboard_on_key_down, on_key_up=self._keyboard_released)

    def _keyboard_released(self, window, keycode):
        self._super = []

    def _keyboard_on_key_down(self, window, keycode, text, super):
        if 'lctrl' in self._super and keycode[1] == 's':
            self._super = []
            self.show_save_dialog()
            return False
        elif 'lctrl' in self._super and keycode[1] == 'l':
            self._super = []
            self.show_load_dialog()
            return False
        elif 'lctrl' in self._super and keycode[1] == 'q':
            self._super = []
            App.get_running_app().stop()
            return False
        elif 'lctrl' not in self._super and keycode[1] in ["lctrl"]:
            self._super.append(keycode[1])
            return False
        else:
            # print("key {} pressed.".format(keycode))
            return False

    def _dismiss_popup(self):
        self._active_popup.dismiss()

    def set_status(self, text, decay=0):
        current_time = datetime.now().strftime("%H:%M:%S")
        self._status_bar.text = current_time + ':  '+text

        if decay > 0:
            Clock.schedule_once(lambda x: self.set_status('status nominal'), decay)

    def _load_data(self, filepath):
        if not os.path.exists(filepath):
            self.set_status('failed to load . file does not exist')

        self.filepath = pt.Path(filepath)

        with zipfile.ZipFile(self.filepath, 'r') as arch:
            with arch.open('data') as f:
                dataset = pd.read_json(f.read())

            with arch.open('meta') as f:
                metadata = json.loads(f.read())

        for ident, item in dataset.iterrows():
            self._body.add_row(name_v=item['name'],
                               density_v=item['density'],
                               volume_v=item['volume'],
                               weight_v=item['weight'],
                               comment_v=item['comment'])

            self._body.max_volume_display.text = str(metadata['max_volume'])
            self._body.max_weight_display.text = str(metadata['max_weight'])

    def show_load_dialog(self):
        content = LoadDialog(onload=self.on_load, oncancel=self._dismiss_popup)
        content.ids.fc.path = '.'
        content.ids.fc.filters = ['*.zip']
        self._active_popup = Popup(title='load ship manifest file', content=content,
                                   size_hint=(0.5, 0.5))
        self._active_popup.open()

    def on_load(self, basepath, selection):
        if len(selection) == 0:
            self.set_status('no file selected!', decay=3)
            return

        filepath = selection[0]
        self._body.clear()
        self._load_data(filepath)
        self._dismiss_popup()
        self._body.update_summary()

    def show_save_dialog(self):
        content = SaveDialog(onsave=self.on_save, oncancel=self._dismiss_popup)
        content.ids.fc.path = self.filepath.parent.absolute().as_posix()
        content.ids.fc.filters = ['*.zip', ]
        content.filename = self.filepath.absolute().as_posix()
        self._active_popup = Popup(title='save ship manifest file', content=content,
                                   size_hint=(0.5, 0.5))
        self._active_popup.open()

    def on_save(self, basepath, filepath):
        if len(filepath) == 0:
            self.set_status('no filename provided for save!', decay=3)
            return

        filepath = pt.Path(filepath)

        if filepath.suffix != '.zip':
            self.set_status('not saved! filename needs to be zip file', decay=3)
            return

        self.save(filepath)

        self._dismiss_popup()

    def save(self, filepath):

        data = self._body.data()
        metadata = {'max_weight': self._body.max_weight_display.text,
                    'max_volume': self._body.max_volume_display.text,
                    }

        backups = ['data.5', 'data.4', 'data.3', 'data.2', 'data.1', 'data']
        out = {}

        if os.path.exists(filepath):
            with zipfile.ZipFile(filepath, 'r') as arch:
                for current_one, next_one in zip(backups[1:], backups[:-1]):
                    if current_one in arch.namelist():
                        out[next_one] = arch.open(current_one).read()

        with zipfile.ZipFile(filepath, 'w') as arch:
            for k, v in out.items():
                arch.writestr(k, v)

            arch.writestr('data', data.to_json(indent=4))

            arch.writestr('meta', json.dumps(metadata))

        self.set_status(f'saved to "{filepath}" ...', 5)

    def clear_editor(self):
        self._body.clear()
        self._body.max_volume_display.text = "0"
        self._body.max_weight_display.text = "0"
        self._body.total_volume_display.text = "0"
        self._body.total_weight_display.text = "0"

    def save_excel(self):
        d = self.__prepare_data()
        d.to_excel('./data.xlsx')
        self.ids.statusbar.text = 'saved to data.xlsx ...'

    def save_markdown(self):
        d = self.__prepare_data()
        s = d[['volume', 'weight']].sum()
        s = s.apply(np.ceil)
        s['name'] = 'total:'
        s['comment'] = 'sum over all commodities'
        s = s.astype(str)
        s['density'] = ''
        s['volume'] = s['volume']+' m^3'
        s['weight'] = s['weight']+' t'
        s.name = 'x'
        d['density'] = d['density'].astype(str)+' t/m^3'
        d['volume'] = d['volume'].astype(str)+' m^3'
        d['weight'] = d['weight'].astype(str)+' t'
        d = d.append(s)
        d.to_markdown('./data.md', index=False)
        self.ids.statusbar.text = 'saved to data.md ...'

    def save_html(self):
        d = self.__prepare_data()
        d.to_html('./data.html')
        self.ids.statusbar.text = 'saved to data.html ...'


class CMEApp(App):
    def build(self):
        Window.size = (1300, 500)
        return CargoManifestEditor()

Factory.register('LoadDialog', LoadDialog)
Factory.register('SaveDialog', SaveDialog)

if __name__ == '__main__':
    Config.set('graphics', 'width', '1000')
    Config.set('graphics', 'height', '500')
    CMEApp().run()


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
